/**
 * Common Operations.js
 *
 * @description     :: To perform common operations  especially ROLLBACK
 * @createdBy       :: Swati
 * @created Date    :: 01/02/2016
 * @Last edited by  :: 
 * @last edited date::
 */

"use strict";

module.exports = {

    /*
	# Method: findRecord
	# Description: to find any record in database
	# Input: Model name,array of IDs to find and callback function
	# Output : response with success or error
  	*/

    findRecord: function(Model, ids, fieldName, token, programId, next) {

        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };
        var Model;
        var queryString = {};

        //switch case defined to match model name
        switch (Model) {
            case 'ProgramUsers':
                Model = ProgramUsers;
                break;
            case 'ProgramRoleConfiguration':
                Model = ProgramRoleConfiguration;
                break;
            case 'NonTransactionalBoosters':
                Model = NonTransactionalBoosters;
                break;
            case 'TransactionalBoosters':
                Model = TransactionalBoosters;
                break;
            case 'BoosterRules':
                Model = BoosterRules;
                break;
            case 'ProgramSetup':
                Model = ProgramSetup;
                break;
            case 'ProgramScheme':
                Model = ProgramScheme;
                break;
            case 'ProgramBenchMarks':
                Model = ProgramBenchMarks;
                break;
            case 'ProgramUniqueCodeSetup':
                Model = ProgramUniqueCodeSetup;
                break;
            case 'RewardsGallerySlabs':
                Model = RewardsGallerySlabs;
                break;
            case 'SpecificRewardsGallery':
                Model = SpecificRewardsGallery;
                break;
            case 'UserProfilingMaster':
                Model = UserProfilingMaster;
                break;
        }

        //switch case defined to match field name
        switch (fieldName) {
            case 'programId':
                queryString = { programId: ids };
                break;
            case 'programRuleId':
                queryString = { programRuleId: ids };
                break;
            case 'boosterId':
                queryString = { boosterId: ids };
                break;
            case 'programUserId':
                queryString = { programUserId: ids };
                break;
            case 'benchmarkId':
                queryString = { benchmarkId: ids };
                break;
            case 'programRoleConfigId':
                queryString = { programRoleConfigId: ids };
                break;
            case 'productId':
                queryString = { productId: ids };
                break;
            case 'boosterRuleId':
                queryString = { boosterRuleId: ids };
                break;
                /* case 'uniqueCodeId':
                     queryString = {uniqueCodeId:ids};
                     break;  */
                /*case 'programId':
                    queryString = {productId:ids};
                    break;  */
            case 'recordId':
                queryString = { recordId: ids };
                break;
            case 'userProfilingId':
                queryString = { userProfilingId: ids };
                break;
        }

        if (programId !== undefined) {
            queryString.programId = programId
        }

        //console.log("queryString", queryString);
        Model.find(queryString).exec(function(error, foundRecords) {
            if (error || !foundRecords) {
                ////console.log("Unable to find record",err,foundRecords);
                responseObj.statusCode = -2;
                responseObj.message = "Fatal Integrity Error : Failed to found records";
                responseObj.result = queryString;
                //sails.log.error("CommonOperations>findRecord>BusinessEntityUserMaster.destroy ","records to be find:"+ids," error:"+error);
                return next(responseObj);
            } else {
                if (foundRecords.length === 0) {
                    responseObj.statusCode = -2;
                    responseObj.message = "Fatal Integrity Error ";
                    responseObj.result = queryString;
                    //sails.log.error("CommonOperations>findRecord ","records to be founds:"+ids," error:"+error);
                    return next(responseObj);
                } else {
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully found";
                    responseObj.result = foundRecords;
                    //sails.log.info("CommonOperations>findRecord>BusinessEntityUserMaster.destroy: record is successfully destroyed");
                    return next(responseObj);
                }
            }
        });
    },



    findRecords: function(Model, queryString, token, next) {

        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };
        var Model;
        //switch case defined to match model name
        switch (Model) {
            case 'ProgramUsers':
                Model = ProgramUsers;
                break;
            case 'ProgramRoleConfiguration':
                Model = ProgramRoleConfiguration;
                break;
            case 'NonTransactionalBoosters':
                Model = NonTransactionalBoosters;
                break;
            case 'TransactionalBoosters':
                Model = TransactionalBoosters;
                break;
            case 'BoosterRules':
                Model = BoosterRules;
                break;
            case 'ProgramSetup':
                Model = ProgramSetup;
                break;
            case 'ProgramScheme':
                Model = ProgramScheme;
                break;
            case 'ProgramBenchMarks':
                Model = ProgramBenchMarks;
                break;
            case 'ProgramUniqueCodeSetup':
                Model = ProgramUniqueCodeSetup;
                break;
            case 'RewardsGallerySlabs':
                Model = RewardsGallerySlabs;
                break;
            case 'SpecificRewardsGallery':
                Model = SpecificRewardsGallery;
                break;
            case 'UserProfilingMaster':
                Model = UserProfilingMaster;
                break;

        }

        //switch case defined to match field name
        /*switch (fieldName) {
            case 'programId':
                queryString = {programId:ids};
                break;
            case 'programRuleId':
                queryString = {programRuleId:ids};
                break;
            case 'boosterId':
                queryString = {boosterId:ids};
                break;
            case 'programUserId':
                queryString = {programUserId:ids};
                break;
            case 'benchmarkId':
                queryString = {benchmarkId:ids};
                break;
            case 'programRoleConfigId':
                queryString = {programRoleConfigId:ids};
                break;  
        }*/

        //console.log("queryString", queryString);
        Model.find(queryString).exec(function(error, foundRecords) {
            if (error || !foundRecords) {
                ////console.log("Unable to find record",err,foundRecords);
                responseObj.statusCode = -2;
                responseObj.message = "Fatal Integrity Error : Failed to found records";
                responseObj.result = queryString;
                //sails.log.error("CommonOperations>findRecord>BusinessEntityUserMaster.destroy ","records to be find:"+ids," error:"+error);
                return next(responseObj);
            } else {
                if (foundRecords.length === 0) {
                    responseObj.statusCode = -2;
                    responseObj.message = "Fatal Integrity Error ";
                    responseObj.result = queryString;
                    //sails.log.error("CommonOperations>findRecord ","records to be founds:"+ids," error:"+error);
                    return next(responseObj);
                } else {
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully found";
                    responseObj.result = foundRecords;
                    //sails.log.info("CommonOperations>findRecord>BusinessEntityUserMaster.destroy: record is successfully destroyed");
                    return next(responseObj);
                }
            }
        });
    },

    //----------------------------------------------------------------------------------------------------------//
    updateRecords: function(Model, fieldName, ids, updateInfo, token, next) {

        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };

        var queryString;

        //switch case defined to match model name
        switch (Model) {
            case 'ProgramUsers':
                Model = ProgramUsers;
                break;
            case 'ProgramRoleConfiguration':
                Model = ProgramRoleConfiguration;
                break;
            case 'NonTransactionalBoosters':
                Model = NonTransactionalBoosters;
                break;
            case 'TransactionalBoosters':
                Model = TransactionalBoosters;
                break;
            case 'BoosterRules':
                Model = BoosterRules;
                break;
            case 'ProgramSetup':
                Model = ProgramSetup;
                break;
            case 'ProgramScheme':
                Model = ProgramScheme;
                break;
            case 'ProgramBenchMarks':
                Model = ProgramBenchMarks;
                break;
            case 'ProgramUniqueCodeSetup':
                Model = ProgramUniqueCodeSetup;
                break;
            case 'RewardsGallerySlabs':
                Model = RewardsGallerySlabs;
                break;
            case 'SpecificRewardsGallery':
                Model = SpecificRewardsGallery;
                break;
            case 'UserProfilingMaster':
                Model = UserProfilingMaster;
                break;

        }

        //switch case defined to match field name
        switch (fieldName) {
            case 'programId':
                queryString = { programId: ids };
                break;
            case 'programRuleId':
                queryString = { programRuleId: ids };
                break;
            case 'boosterId':
                queryString = { boosterId: ids };
                break;
            case 'programUserId':
                queryString = { programUserId: ids };
                break;
            case 'benchmarkId':
                queryString = { benchmarkId: ids };
                break;
            case 'programRoleConfigId':
                queryString = { programRoleConfigId: ids };
                break;
            case 'programRuleId':
                queryString = { programRuleId: ids };
                break;
            case 'boosterRuleId':
                queryString = { boosterRuleId: ids };
                break;
            case 'uniqueCodeId':
                queryString = { uniqueCodeId: ids };
                break;
            case 'rewardsGallerySlabsId':
                queryString = { rewardsGallerySlabsId: ids };
                break;
            case 'recordId':
                queryString = { recordId: ids };
                break;
            case 'userProfilingId':
                queryString = { userProfilingId: ids };
                break;
        }

        //console.log("queryString", queryString);
        //console.log("updateInfo", updateInfo);

        Model.update(queryString, updateInfo).exec(function(error, updatedRecords) {
            ////console.log("updatedRecords",updatedRecords);
            if (error || !updatedRecords) {
                responseObj.statusCode = -1;
                responseObj.message = "Record Updation Failed" + error;
                sails.log.error("CommonOperations>updateRecord>" + Model + ".update ", "records to be updated:" + ids, " error:" + error);
                next(responseObj);
            } else {
                if (updatedRecords.length === 0) { // no records found or some error
                    responseObj.statusCode = 2;
                    responseObj.message = "No Record found!!!";
                    sails.log.error("CommonOperations>updateRecord>" + Model + ".update ", "records to be updated:" + ids, " error:" + error);
                    next(responseObj);
                } else { // success: records updated
                    ////console.log("updated records is",updatedRecords);
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully updated";
                    sails.log.info("CommonOperations>updateRecord>" + Model + ".update: record is successfully updated");
                    next(responseObj);
                }
            }
        });
    },
    //----------------------------------------------------------------------------------------------------------//

    /*
    # Method: deleteRecord
    # Description: to delete any record in database
    # Input: Model name,array of IDs to delete and callback function
    # Output : response with success or error
    */
    deleteRecord: function(Model, fieldName, ids, token, next) {

        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };

        var queryString;
        switch (Model) {
            case 'ProgramUsers':
                Model = ProgramUsers;
                break;
            case 'ProgramRoleConfiguration':
                Model = ProgramRoleConfiguration;
                break;
            case 'NonTransactionalBoosters':
                Model = NonTransactionalBoosters;
                break;
            case 'TransactionalBoosters':
                Model = TransactionalBoosters;
                break;
            case 'BoosterRules':
                Model = BoosterRules;
                break;
            case 'ProgramSetup':
                Model = ProgramSetup;
                break;
            case 'ProgramScheme':
                Model = ProgramScheme;
                break;
            case 'ProgramBenchMarks':
                Model = ProgramBenchMarks;
                break;
            case 'ProgramUniqueCodeSetup':
                Model = ProgramUniqueCodeSetup;
                break;
            case 'RewardsGallerySlabs':
                Model = RewardsGallerySlabs;
                break;
            case 'SpecificRewardsGallery':
                Model = SpecificRewardsGallery;
                break;
            case 'UserProfilingMaster':
                Model = UserProfilingMaster;
                break;
        }

        //switch case defined to match field name
        switch (fieldName) {
            case 'programId':
                queryString = { programId: ids };
                break;
            case 'programRuleId':
                queryString = { programRuleId: ids };
                break;
            case 'boosterId':
                queryString = { boosterId: ids };
                break;
            case 'programUserId':
                queryString = { programUserId: ids };
                break;
            case 'benchmarkId':
                queryString = { benchmarkId: ids };
                break;
            case 'programRoleConfigId':
                queryString = { programRoleConfigId: ids };
                break;
            case 'programRuleId':
                queryString = { programRuleId: ids };
                break;
            case 'boosterRuleId':
                queryString = { boosterRuleId: ids };
                break;
            case 'uniqueCodeId':
                queryString = { uniqueCodeId: ids };
                break;
            case 'rewardsGallerySlabsId':
                queryString = { rewardsGallerySlabsId: ids };
                break;
            case 'recordId':
                queryString = { recordId: ids };
                break;
            case 'userProfilingId':
                queryString = { userProfilingId: ids };
                break;
        }

        Model.destroy(queryString).exec(function(error, deletedRecords) {
            if (error || !deletedRecords) {
                ////console.log("Unable to delete record",err,deletedRecords);
                responseObj.statusCode = -2;
                responseObj.message = "Fatal Integrity Error : Failed to delete record in case of rollback";
                responseObj.result = businessId;
                sails.log.error("CommonOperations>deleteRecord>" + Model + ".destroy >records to be deleted:" + ids, " error:" + error);
                return next(responseObj);
            } else {
                if (deletedRecords.length === 0) {
                    responseObj.statusCode = -2;
                    responseObj.message = "Fatal Integrity Error : Failed to delete record in case of rollback";
                    sails.log.error("CommonOperations>deleteRecord>" + Model + ".destroy > records to be deleted:" + ids, " result:" + deletedRecords);
                    return next(responseObj);
                } else { // success: records deleted
                    ////console.log("deleted records is",deletedRecords);
                    responseObj.statusCode = 0;
                    responseObj.message = "Record successfully deleted";
                    sails.log.info("CommonOperations>deleteRecord>" + Model + ".destroy > record is successfully destroyed");
                    return next(responseObj);
                }
            }
        });
    },
    //----------------------------------------------------------------------------------------------------------//

    /*
    # Method: findCount
    # Description: to find count of all records in a collection
    # Input: Model name,token,callback function
    # Output : response with success or error
    */

    findCount: function(Model, queryString, token, next) {
        // default response object
        var responseObj = {
            statusCode: -1,
            message: null,
            result: null
        };


        var collectionName = Model;
        ////console.log("Finding Count of:",Model);


        switch (Model) {
            case 'ProgramUsers':
                Model = ProgramUsers;
                break;
            case 'ProgramRoleConfiguration':
                Model = ProgramRoleConfiguration;
                break;
            case 'NonTransactionalBoosters':
                Model = NonTransactionalBoosters;
                break;
            case 'TransactionalBoosters':
                Model = TransactionalBoosters;
                break;
            case 'BoosterRules':
                Model = BoosterRules;
                break;
            case 'ProgramSetup':
                Model = ProgramSetup;
                break;
            case 'ProgramScheme':
                Model = ProgramScheme;
                break;
            case 'ProgramUniqueCodeSetup':
                Model = ProgramUniqueCodeSetup;
                break;
            case 'Leads':
                Model = Leads;
                break;
        }


        Model.count(queryString).exec(function(err, foundCount) {
            //console.log(err, foundCount);
            if (err || !foundCount) {
                responseObj.message = "Count cound not be fetched: " + err;
                //console.log("Finding count Failure:", err);
                sails.log.error("CommonOperations > findCount > " + collectionName + "Error" + err);
            } else {
                ////console.log("counts found:",foundCount);
                responseObj.statusCode = 0;
                responseObj.result = foundCount;
                responseObj.message = "counts fetched successfully";
                sails.log.info("CommonOperations > findCount > " + collectionName + "Counts fetched successfully");
            }
            next(responseObj);
        });

    }


};