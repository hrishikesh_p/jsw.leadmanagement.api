/**
 * LeadsController
 *
 * @description :: Server-side logic for managing Leads
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var request = require('request');

module.exports = {

    /*
  	# Method: addSecondarySale
  	# Description: to add primary sales in database
  	*/
    addLead: function(req, res) {
        var token = req.token;
        var programInfo = req.programObject;

        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        userLogObject.requestApi = req.baseUrl + req.path;
        userLogObject.event = "Add Secondary Sale";
        userLogObject.eventType = "Add";

        Leads.addLead(req.body, programInfo, token, userLogObject, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // userLogObject.responseData = response;
            // LogService.addUserLog(userLogObject, token);
        });
    },

    /*
  	# Method: getLeads
  	# Description: to fetch secondary sales from database based on filter
  	*/
    getLeads: function(req, res) {
        console.log("req.body", req.body);
        var token = req.token;
        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        userLogObject.requestApi = req.baseUrl + req.path;
        userLogObject.event = "Fetch secondary sales";
        userLogObject.eventType = "Get";

        Leads.getLeads(req.body, token, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },

    /*
     	# Method: updateLead
     	# Description: to update Lead
     	*/
    updateLead: function(req, res) {
        var token = req.token;
        var programInfo = req.programObject;

        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        userLogObject.requestApi = req.baseUrl + req.path;
        userLogObject.event = "Update Secondary sales";
        userLogObject.eventType = "Update";

        Leads.updateLead(req.body, token, userLogObject, function(response) {
            res.json(response);
            // var resObj = JSON.stringify(response);
            // resObj = JSON.parse(resObj);
            // userLogObject.responseData = resObj;
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },


    /*
    	# Method: get Hall of Fame for Engineers
    	# Description: to fetch Hall of Fame for Engineers based on number of leads uploaded.
    	*/
    getHallOfFame: function(req, res) {
        console.log("req.body", req.body);
        var token = req.token;
        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        userLogObject.requestApi = req.baseUrl + req.path;
        userLogObject.event = "Fetch secondary sales";
        userLogObject.eventType = "Get";

        Leads.getHallOfFame(req.body, token, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },




    /*
  	# Method: countLeads
  	# Description: to count leads from database based on filter
  	*/
    countLeads: function(req, res) {
        console.log("req.body", req.body);
        var token = req.token;
        //Variable defined to create userLogObject
        var userLogObject = req.body.frontendUserInfo;
        userLogObject.requestApi = req.baseUrl + req.path;
        userLogObject.event = "Fetch Leads Count";
        userLogObject.eventType = "Get";

        Leads.countLeads(req.body, token, function(response) {
            res.json(response);
            // userLogObject.response = response.message;
            // LogService.addUserLog(userLogObject, token);
        });
    },























    //Master APIs
    // getMasterSMSE: function(req, res) {
    //     var robj = { msg: "getMasterSMSE", code: "000" };
    //     sails.log.info("getMasterSMSE");
    //     LMSMasterSMSE.getMasterSMSE(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },
    // getMasterSEInfluencer: function(req, res) {
    //     var robj = { msg: "getMasterSEInfluencer", code: "001" };
    //     sails.log.info("getMasterSEInfluencer");
    //     LMSMasterSEInfluencer.getMasterSEInfluencer(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },
    // getMasterPinSE: function(req, res) {
    //     var robj = { msg: "getMasterPinSE", code: "002" };
    //     sails.log.info("getMasterPinSE");
    //     LMSMasterPinSE.getMasterPinSE(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },


    // Leads APIs
    // getLeads: function(req, res) {
    //     var robj = { msg: "getLeads", code: "200" };
    //     sails.log.info("getLeads");
    //     Leads.getLeads(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getLeadsSummary: function(req, res) {
    //     var robj = { msg: "getLeadsSummary", code: "201" };
    //     sails.log.info("getLeadsSummary");
    //     Leads.getLeadsSummary(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getLead: function(req, res) {
    //     var robj = { msg: "getLead", code: "202" };
    //     var index = req.param('id');
    //     sails.log.info("getLead?id=" + index);
    //     Leads.getLead(index, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // addLead: function(req, res) {
    //     var robj = { msg: "addLead", code: "203" };
    //     var obj = req.body;
    //     sails.log.info("addLead");
    //     sails.log.info(obj);
    //     Leads.addLead(obj, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // updateLead: function(req, res) {
    //     var robj = { msg: "updateLead", code: "204" };
    //     var id = req.param("id");
    //     var obj = req.body;
    //     sails.log.info("updateLead for:" + id);
    //     sails.log.info("Body:" + JSON.stringify(obj));
    //     sails.log.info("Body:" + JSON.stringify(obj.sales));
    //     sails.log.info("soruce:" + JSON.stringify(obj.source));
    //     var source = obj.source;
    //     var salesArray = obj.sales
    //     Leads.updateLead(id, obj, function(resp) {
    //         if (source != undefined) {
    //             if (source == "sales") {

    //                 var purchaseDetails = [];
    //                 obj = {
    //                     "productId": "1223",
    //                     "productName": "SKU",
    //                     "packagingUnitId": "GRASS",
    //                     "packagingUnit": "SKU",
    //                     "quantity": 2,
    //                     "MRP": 23,
    //                     "discount": 0,
    //                     "totalPrice": 2
    //                 }
    //                 purchaseDetails.push(obj)
    //             }
    //             //console.log(JSON.stringify(purchaseDetails))
    //             reqObj = {
    //                 "recordInfo": {
    //                     "purchaseDetails": purchaseDetails,
    //                     "creditPartyId": 'BE1500988068759', //"BE1498548448125",
    //                     "debitPartyId": 'BE1500988068759', //"BE1500988068759",
    //                     "invoiceNumber": 12,
    //                     "invoiceAmount": 2342,
    //                     "invoiceAmountExcludingVAT": 232,
    //                     "transactionDate": new Date(),
    //                     "vatPercentage": "0",
    //                     "invoiceDocument": "https://phoenix2-content-files.s3.ap-south-1.amazonaws.com/Operations-Data/invoiceUpload1500989080430.jpg"
    //                 },
    //                 "uploadedFrom": "adminApp",
    //                 "programId": "PR1498035372717",
    //                 "clientId": "CL1496149805465",
    //                 "frontendUserInfo": { "appType": "Grass Apparels" },
    //                 "serviceType": "programOperations",
    //                 "parentUsers": ["BE1498036223421"]
    //             }

    //             // console.log(ServConfigService.getApplicationAPIs().addSecondarySale.url)
    //             //   console.log(JSON.stringify(reqObj));
    //             // apiURL = ServConfigService.getApplicationConfig().base_url +
    //             //     ":" +
    //             //     ServConfigService.getApplicationAPIs().addSecondarySale.port +
    //             //     ServConfigService.getApplicationAPIs().addSecondarySale.url
    //             var requestOptions = {
    //                 url: 'https://localhost:1353/addSecondarySale/v1',
    //                 method: 'post', //ServConfigService.getApplicationAPIs().addSecondarySale.method,
    //                 headers: {
    //                     'authorization': 'Bearer ' + 'eyJhbGciOiJIUzUxMiIsInR5cCI6IkpXVCJ9.eyJpZCI6IkFVMTQ4MzcwMTk4OTA0MiIsImlhdCI6MTQ4NDM3NzMwN30.EVpRt0prqIlEzZ-JwzMHAvmPTKrgjm0DagRS5zm0ivGRSODRRHdPbvwJuqs7ZwKnLPQychB1_2lKeq7ALp96PQ'
    //                 },
    //                 json: reqObj
    //             };

    //             request(requestOptions, function(error, response, body) {
    //                 var responseObj = {};
    //                 if (error || body === undefined) {
    //                     console.log("error ", error, "body ", body)
    //                     responseObj.statusCode = 2;
    //                     responseObj.message = "Error occurred while fetching records from program users!!";
    //                     next(responseObj);
    //                 } else {
    //                     if (body.statusCode === 0) {
    //                         console.log("records 0");
    //                         responseObj.statusCode = 0;
    //                         responseObj.message = "Records fetched  successfully !!";
    //                         console.log("user info is", responseObj.result);
    //                         responseObj.result = body.result;
    //                     } else if (body.statusCode === 2) {
    //                         console.log("records failed");
    //                         responseObj.statusCode = 2;
    //                         responseObj.message = "Failed to fetch records";
    //                     } else {
    //                         console.log("records", body);
    //                         responseObj = response.body;
    //                     }
    //                     // next(responseObj);
    //                 }

    //             })
    //         }

    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // deleteLead: function(req, res) {
    //     var robj = { msg: "deleteLead", code: "205" };
    //     var id = req.param("id");
    //     sails.log.info("deleteLead for:" + id);
    //     Leads.deleteLead(id, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // visitReport: function(req, res) {
    //     var robj = { msg: "visitReport", code: "206" };
    //     sails.log.info("visitReport");
    //     Leads.visitReport(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // influencerReport: function(req, res) {
    //     var robj = { msg: "influencerReport", code: "207" };
    //     sails.log.info("influencerReport");
    //     Leads.influencerReport(function(resp) {
    //         var robj = [];
    //         for (var i = 0; i < resp.length; i++) {
    //             var leadsubmitted = resp[i].lead.length;
    //             var pointsearned = 0;
    //             var leadsqualified = 0;
    //             var lastleadsubmitdate = null;
    //             var lastqualifiedleaddate = null;
    //             for (var j = 0; j < resp[i].lead.length; j++) {
    //                 pointsearned = pointsearned + parseInt(resp[i].lead[j].weight, 10);
    //                 var ldate = new Date(resp[i].lead[j].createdate);
    //                 if (resp[i].lead[j].qualified === "true") {
    //                     leadsqualified = leadsqualified + 1;
    //                     if (lastqualifiedleaddate === null) {
    //                         lastqualifiedleaddate = ldate;
    //                     } else {
    //                         if (lastqualifiedleaddate <= ldate) {
    //                             lastqualifiedleaddate = ldate;
    //                         }
    //                     }
    //                 }
    //                 //last submit date
    //                 if (lastleadsubmitdate === null) {
    //                     lastleadsubmitdate = ldate;
    //                 } else {
    //                     if (lastleadsubmitdate <= ldate) {
    //                         lastleadsubmitdate = ldate;
    //                     }
    //                 }
    //             }
    //             var obj = {
    //                 id: resp[i]._id.influencer,
    //                 leadsubmitted: leadsubmitted,
    //                 pointsearned: pointsearned,
    //                 leadsqualified: leadsqualified,
    //                 lastleadsubmitdate: lastleadsubmitdate,
    //                 lastqualifiedleaddate: lastqualifiedleaddate
    //             };
    //             robj.push(obj);
    //         }
    //         sails.log.info(robj);
    //         res.json(robj);
    //     });
    // },


    // //Influencer APIs
    // getInfluencers: function(req, res) {
    //     var robj = { msg: "getInfluencers", code: "300" };
    //     sails.log.info("getInfluencers");
    //     LMSUser.getInfluencers(function(resp) {
    //         sails.log.info(resp);
    //         sails.log.info(resp[0].name + resp[0].contact);
    //         res.json(resp);
    //     });
    // },

    // getInfluencersString: function(req, res) {
    //     var robj = { msg: "getInfluencers", code: "300" };
    //     sails.log.info("getInfluencers");
    //     LMSUser.getInfluencers(function(resp) {
    //         sails.log.info(resp);
    //         var sresp = [];
    //         for (let i = 0; i < resp.length; i++) {
    //             sresp.push(resp[i].id + ":" + resp[i].name + ":" + resp[i].contact);
    //         }
    //         sails.log.info(sresp);
    //         res.json(sresp);
    //     });
    // },

    // getInfluencer: function(req, res) {
    //     var robj = { msg: "getInfluencer", code: "301" };
    //     var index = req.param('id');
    //     sails.log.info("getInfluencer?id=" + index);
    //     LMSUser.getInfluencer(index, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getInfluencerMap: function(req, res) {
    //     var robj = { msg: "getInfluencerMap", code: "302" };
    //     var pids = req.param('id').split(",");
    //     /*var jids = {ids:JSON.parse(pids)};
    //     sails.log.info("getInfluencerMap=" + jids.ids.length);
    //     var qids = [];
    //     for(let i=0;i<jids.ids.length;i++) {
    //         sails.log.info(jids[i]);
    //         qids.push(jids[i].iid);
    //     }*/
    //     sails.log.info("getInfluencerMap=" + pids);
    //     LMSUser.getInfluencerMap(pids, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // //ServiceUsers APIs
    // getServiceUsers: function(req, res) {
    //     var robj = { msg: "getServiceUsers", code: "400" };
    //     sails.log.info("getServiceUsers");
    //     LMSUser.getServiceUsers(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getServiceUser: function(req, res) {
    //     var robj = { msg: "getServiceUser", code: "401" };
    //     var index = req.param('id');
    //     sails.log.info("getServiceUser?id=" + index);
    //     LMSUser.getServiceUser(index, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // //SalesUsers APIs
    // getSalesUsers: function(req, res) {
    //     var robj = { msg: "getSalesUsers", code: "600" };
    //     sails.log.info("getSalesUsers");
    //     LMSUser.getSalesUsers(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getSalesUser: function(req, res) {
    //     var robj = { msg: "getSalesUser", code: "601" };
    //     var index = req.param('id');
    //     sails.log.info("getSalesUser?id=" + index);
    //     LMSUser.getSalesUser(index, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // //DearlerUsers APIs
    // getDealerUsers: function(req, res) {
    //     var robj = { msg: "getDealerUsers", code: "800" };
    //     sails.log.info("getDealerUsers");
    //     LMSUser.getDealerUsers(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // getDealerUser: function(req, res) {
    //     var robj = { msg: "getDealerUser", code: "801" };
    //     var index = req.param('id');
    //     sails.log.info("getDealerUser?id=" + index);
    //     LMSUser.getDealerUser(index, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // //Login Users APIs
    // getLoginUsers: function(req, res) {
    //     var robj = { msg: "getLoginUsers", code: "900" };
    //     sails.log.info("getLoginUsers");
    //     LMSUser.getLoginUsers(function(resp) {
    //         console.log("resp" + JSON.stringify(resp))
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },

    // //Template APIs
    // getVisitTemplates: function(req, res) {
    //     var robj = { msg: "getVisitTemplates", code: "1000" };
    //     sails.log.info("getVisitTemplates");
    //     LMSVisitTemplate.getLMSVisitTemplates(function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // },
    // addVisitTemplate: function(req, res) {
    //     var robj = { msg: "addVisitTemplate", code: "1001" };
    //     var obj = req.body;
    //     sails.log.info("addVisitTemplate");
    //     sails.log.info(obj);
    //     LMSVisitTemplate.addLMSVisitTemplate(obj, function(resp) {
    //         sails.log.info(resp);
    //         res.json(resp);
    //     });
    // }
};