/**
 * isAuthorized
 * @description      :: Policy to check if user if user exist or not
 * @created          :: Swati Prajapati
 * @Created  Date    :: 08/03/2017
 */

"use strict";

var request = require('request');

//code to allow self signed certificates
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};

module.exports = function(req, res, next) {
    // console.log("req<<<<", );
    //Object to send response
    var responseObj = {
        statusCode: -1,
        message: null,
        result: null
    };
    var apiURL; //variable defined for url
    var reqObject; //request object defined for service call
    var requestOptions; //paramters defined for service call
    //var userId = req.body.userId;
    var userId = req.body.frontendUserInfo.userId;
    var frontendUserInfo = req.body.frontendUserInfo;
    var token = req.token; //
    var appType = req.body.frontendUserInfo.appType;
    if (req.body.isApproved === undefined || req.body.isApproved === false) {
        req.isApproved = false;
    } else {
        req.isApproved = true;
    }
    req.userInfo = {};

    var i;
    var j;
    var screens;

    apiURL = ServConfigService.getApplicationConfig().base_url +
        ":" +
        ServConfigService.getApplicationAPIs().getAnnectosUser.port +
        ServConfigService.getApplicationAPIs().getAnnectosUser.url

    reqObject = { "userId": userId, frontendUserInfo: frontendUserInfo };
    //console.log("req>>>>", reqObject);
    requestOptions = {
        url: apiURL,
        method: ServConfigService.getApplicationAPIs().getAnnectosUser.method,
        headers: {
            'authorization': 'Bearer ' + token
        },
        json: reqObject
    };

    if (appType !== undefined) {
        request(requestOptions, function(error, response, body) {
            console.log("DOES USER EXIST Service called");
            console.log("body", body);
            if (error || body === undefined) {
                responseObj.message = "Error occurred in InternalRoles Service";
                return res.json(responseObj);
            } else if (body.statusCode === 0) {
                req.userInfo = body.result;
                responseObj = body;

                var applicationAccess = req.userInfo.applicationAccess;
                var additionalAccess = req.userInfo.additionalAccess;
                var isApprovalModuleExist = false; //default flag set to check for approval model in application exist

                if (applicationAccess) {
                    for (i = 0; i < applicationAccess.length; i++) {
                        if (applicationAccess[i].moduleCode === "m3") {

                            screens = applicationAccess[i].screens
                            for (j = 0; j < screens.length; j++) {
                                if (screens[j].screenCode === "m3s8") {
                                    req.isApproved = true;
                                    isApprovalModuleExist = true;
                                    break;
                                }
                            }
                            if (isApprovalModuleExist) {
                                break;
                            }
                        }
                    }
                }
                //If approval module not found in applicationAccess then search in additionalAccess
                if (!isApprovalModuleExist) {
                    if (additionalAccess != undefined) {
                        for (i = 0; i < additionalAccess.length; i++) {

                            screens = additionalAccess[i].screens
                            for (j = 0; j < screens.length; j++) {
                                if (screens[j].screenCode === "m3s8") {
                                    req.isApproved = true;
                                    isApprovalModuleExist = true;
                                    break;
                                }
                            }
                            if (isApprovalModuleExist) {
                                break;
                            }
                        }
                    }
                }
                next();
            } else {
                responseObj.message = body;
                return res.json(responseObj);
            }
        })
    } else {
        req.userInfo.userId = userId;
        req.isApproved = true;
        next();
    }


}