/**
 * doesProgramExist
 * @description      :: Policy to check if program exist or not
 * @created          :: Swati Prajapati
 * @Created  Date    :: 02/02/2017
 * @lastEdited       :: Swati Prajapati
 * @lastEdited Date  :: 02/02/2017
 */

"use strict";

var request = require('request');

// code to accept self-signed certifcate
if ('development' == sails.config.environment) {
    process.env.NODE_TLS_REJECT_UNAUTHORIZED = "0";
};

module.exports = function(req, res, next) {

    var responseObj = {
        "statusCode": -1,
        "message": null,
        "result": null
    };

    if (req.body) {
        var programId = req.body.programId;
    }
    console.log("programId", programId);
    //console.log("req.body", req.body);

    var token = req.token;
    //api to get all program users
    var apiURL = ServConfigService.getApplicationConfig().base_url +
        ":" +
        ServConfigService.getApplicationAPIs().getAllProgramFromMaster.port +
        ServConfigService.getApplicationAPIs().getAllProgramFromMaster.url;

    //request object
    var requestObject = { programId: programId, frontendUserInfo: req.body.frontendUserInfo };

    //request options to fetch program user
    var requestOptions = {
        url: apiURL,
        method: ServConfigService.getApplicationAPIs().getAllProgramFromMaster.method,
        headers: {
            'authorization': 'Bearer ' + token
        },
        json: requestObject
    };

    // making the service call to get program users
    request(requestOptions, function(error, response, body) {
        //                //console.log("Error is:",error,"response body is:",body);

        if (error || body === undefined) { // error occured
            responseObj.message = "Error occured in fetching program:" + error;
            res.json(200, responseObj);

        } else {
            if (body.statusCode === 0) { // program found succesfully
                responseObj = body;
                req.programObject = body.result[0];
                //console.log("Program found !!!", programId);
                next();
            } else {
                responseObj.message = "Program not found";
                responseObj.statusCode = body.statusCode;
                //console.log("Program not found");
                res.json(200, responseObj);
            }
        }
    });
    // end of request

};